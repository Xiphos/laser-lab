import datetime,sys,os,re

class token:
    name=''
    pattern = None
    value = 'None'

    def __init__(self, name, pattern):
        self.name = name
        self.pattern = pattern
        self.value = 'None'

    def match(self, string):
        potentialMatch = self.pattern.match(string)

        ##Check if a match was found and it is valid for our parameter
        ##Do not set a value if we already have one
        if potentialMatch and (self.value == 'None'):
            potentialValue = potentialMatch.groups()[0]
            self.validate(potentialValue)

    ##validate() parses and sets the value
    def validate(self, value):
        self.value = value
        return True

##Derived classes
class filePathToken(token):

    ##Take anything as a filepath
    def validate(self,value):
        ##Append date to logfile
        if self.name == 'logfile':
            pat = re.compile(r'(.*)\.(.*)$')
            if(pat.match(value)): ##if the filepath is [].[]
                filePathLead = pat.match(value).groups()[0]
                datestamp = datetime.datetime.now().strftime("[%y-%m-%d][%H:%M:%S]")
                filePathExtension = pat.match(value).groups()[1]

                newPath = filePathLead + datestamp + "." + filePathExtension

                self.value = newPath
            else:
                self.value = value
        else:
            self.value = value
        
        return True

class watchdogToken(token):

    def validate(self, value):
        pat = re.compile(r'^(\d+)$')
        if pat.match(value):
            timeout = (int)(pat.match(value).groups()[0])

            ##Valid watchdog times are 1-15
            if timeout <= 15 and timeout >= 1:
                self.value = value
            else:
                self.value = 'None'

class outputmodeToken(token):

    def validate(self, value):
        upper = value.upper()
        outputEnum = ['NONE', 'BASIC', 'FULL']
        if upper in outputEnum:
            self.value = value
        else:
            self.value = 'None'
