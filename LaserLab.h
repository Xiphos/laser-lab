#ifndef LSR_LAB_H
#define LSR_LAB_H

#include <stdlib.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>		//for the printf() function
#include <fcntl.h>
#include <linux/watchdog.h> 	//needed for the watchdog specific constants
#include <unistd.h> 		//needed for sleep
#include <sys/ioctl.h> 		//needed for the ioctl function
#include <stdlib.h> 		//for atoi
#include <time.h> 		//for time_t and the time() function
#include <sys/time.h>           //for gettimeofday()
#include <stdbool.h>
#include <wiringPi.h>
#include <string.h>

#define LASER1 0 //Pin for Laser 1
#define LASER2 3 //Pin for laser 2
#define SLEEPLENGTH 10000 //Time for small sleeps
#define WATCHDOGTIMEOUT_MAX 15 //Maximum watchdog time
#define WATCHDOGTIMEOUT_MIN 1 //Minimum watchdog time
#define WATCHDOGTIMEOUT_DEFAULT 10 //Default watchdog time
#define PRINT_MSG(file, cTime, str, outputMode) \
    do{ \
        fprintf(file, "(%s) %s\n", cTime, str);\
        if(outputMode == FULL)\
            fprintf(stderr,  "(%s) %s\n", cTime, str);\
        fflush(file);\
    }while(0)
#define STATISTICSUPDATE 2 //How often to write to the statistics file
#define STATUSUPDATE 30 //How often to write a status update to the logfile

//Default config parameters
#define LOGFILEPATH "./LaserLab.log"
#define CONFIGFILEPATH "./LaserLab.cfg"
#define STATSFILEPATH "./LaserLab.stats"
#define OUTPUTMODEDEFAULT FULL
#define NUMBEROFCONFIGPARAMETERS 4

#define CONFIGPARAMETERDEFAULT "None"

//Enums
enum LaserState {BROKEN, UNBROKEN};
typedef enum State {WAITING, START, L1BROKEN, L2BROKEN, PARTIALIN, PARTIALOUT, GOINGIN, GOINGOUT} State;
typedef enum CfgFileState {CFGSTART, WHITESPACE, GOT_ALHPANUMERIC,GOT_EQUALSIGN, GOT_PARAMETER_NAME, PARAMETER_DONE, GOT_VALUE, CFGDONE, CFGEXIT} CfgFileState;
typedef enum OutputMode {NONE, BASIC, FULL} OutputMode;
//Function declarations
int main(const int argc, char* argv[]);

//void readConfig(FILE* configFile, int* timeout, char* logFileName, char* StatsFile);
void getTime(char* buffer);
bool outputStats(const char* statsFile, const int l1Broken, const int l2Broken, const int numIn, const int numOut, const char* cTime);
void printAllGpio();
int setupWatchdog(int timeout, char* cTime, FILE* logFile, OutputMode outputMode);
#endif
