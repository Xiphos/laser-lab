#!/bin/bash

sudo mv *.py ../pyScripts/ ##Move all the scripts over to the other directory
cd ../pyScripts ##go to the directory
sudo chmod +x sentinel.py ##Allow the script to be executed

##Move everything over
sudo cp sentinel.py /usr/bin/labSentinel
sudo cp readConfig.py /usr/bin/readConfig.py
sudo cp configToken.py /usr/bin/configToken.py