\contentsline {chapter}{\numberline {1}Introduction}{2}{chapter.1}
\contentsline {section}{\numberline {1.1}Project Overview}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}High Level System Design}{2}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Software}{2}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Hardware}{3}{subsection.1.2.2}
\contentsline {chapter}{\numberline {2}Software Design}{4}{chapter.2}
\contentsline {section}{\numberline {2.1}Overview}{4}{section.2.1}
\contentsline {section}{\numberline {2.2}Function Descriptions}{5}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Main Program Functions}{5}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}gpiolib\_pinfuncs Functions}{7}{subsection.2.2.2}
\contentsline {section}{\numberline {2.3}System Dependence}{7}{section.2.3}
\contentsline {section}{\numberline {2.4}The Main State Machine}{8}{section.2.4}
\contentsline {section}{\numberline {2.5}Configuration File}{9}{section.2.5}
\contentsline {section}{\numberline {2.6}Log File}{9}{section.2.6}
\contentsline {section}{\numberline {2.7}Watchdog}{9}{section.2.7}
\contentsline {chapter}{\numberline {3}Further Information}{10}{chapter.3}
\contentsline {section}{\numberline {3.1}Testing}{10}{section.3.1}
\contentsline {section}{\numberline {3.2}Miscellaneous}{10}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Extras}{10}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Limitations}{11}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Reflection}{11}{subsection.3.2.3}
\contentsline {chapter}{\numberline {4}Appendix}{12}{chapter.4}
\contentsline {section}{\numberline {4.1}Source Code}{12}{section.4.1}
\contentsline {section}{\numberline {4.2}Peer Contributions}{12}{section.4.2}
