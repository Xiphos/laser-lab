import sys, os, re
import configToken as token

##This program will read the config file and put it in a format that 
def getConfigPath():
    ##THIS ONLY WORKS WHEN THE H FILE IS PRESENT##
    cfgfl = re.compile(r'^#define\sCONFIGFILEPATH\s"(.*)"$') #configfile path define

    with open('LaserLab.h', 'r') as headerFile:
        file = headerFile.readlines()
        for line in file:
            match = re.match(cfgfl, line)
            if match:
                return match.groups()[0]
        
    return None

def readConfig(path, parameters):
    ##iterate over the file and try to match the variables
    with open(path, 'r') as configFile:
        file = configFile.readlines()
        for line in file:

            ##Try to match each parameter with the line
            for param in parameters:
                param.match(line)
    
    return True

def printLog(logfilePath, msg):
    with open(logfilePath, 'a') as f:
        f.write(msg)
        f.close()

def writeDefaultConfig(path):

    defaultLines = [
        'WATCHDOG_TIMEOUT = 10\n',
        'LOGFILE = ./logfiles/Laser.log\n',
        'STATSFILE = ./laserLab.stats\n',
        'OUTPUTMODE = FULL\n'
    ]
    with open(path, 'w+') as f:
        f.writelines(defaultLines)
    f.close()

def main():
    params = ["log", "config", "stats", "outputmode"]
    cfgPath = "./LaserLab.cfg"

    ##If we couldn't find the default config path
    if not cfgPath:
        print("ERROR: No config file path defined in LaserLab.h!")
        exit

    ##If the file doesnt exist
    if not os.path.exists(cfgPath):
        print("WARNING: No config file exists, creating a default")
        writeDefaultConfig(cfgPath)

    watchdog = token.watchdogToken('watchdog', re.compile(r'^watchdog_timeout(?:\s*?)=(?:\s*?)(\d*)$', re.IGNORECASE))
    logfile = token.filePathToken('logfile', re.compile(r'^logfile(?:\s*?)=(?:\s*?)(.*)$', re.IGNORECASE))
    statsfile = token.filePathToken('statsfile', re.compile(r'^statsfile(?:\s*?)=(?:\s*?)(.*)$', re.IGNORECASE))
    outputmode = token.outputmodeToken('outputmode',re.compile(r'^outputmode(?:\s*?)=(?:\s*?)(\w*)$',re.IGNORECASE))

    params = [watchdog, logfile, statsfile, outputmode]
    configDict = readConfig(cfgPath, params)
    out = []
    for param in params:
        out.append(param)
    return out
