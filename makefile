##Macros##
COMPILER=/opt/cross-pi-gcc/bin/arm-linux-gnueabihf-gcc
FLAGS= -Wall -L/usr/local/lib -I/usr/local/include -lm -lwiringPi -lpthread
SOURCES := LaserLab.c

##targets##
all: LaserLab.c LaserLab.h Build
send: all
	scp build/* s5khatta@ecelinux4.uwaterloo.ca:laserLab/
sendpi: all
	scp build/* pi@192.168.1.135:Embedded_Lab/ ##Move the main exec to the folder
Build: ${SOURCES}
	$(COMPILER) -o build/laserLab ${SOURCES} ${FLAGS}
	cp *.py build/
