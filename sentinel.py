#!/usr/bin/python
import sys, os, readConfig, subprocess, signal, time
import threading

class LaserLab():
    execute = None
    proc = None
    def __init__(self, cmd):
        self.execute = cmd
    
    def realtimeOutput(self):
        self.proc = subprocess.Popen(self.execute, shell=True, stdin= subprocess.PIPE, stderr = sys.stderr, stdout = subprocess.PIPE)
        while True:
            try:
                out = self.proc.stderr.readline()
                if out == '' and self.process.poll() is not None:
                    break
                elif out:
                    self.output_ctrl.write(out)
            except:
                pass
    def killProc(self):
        self.proc.terminate()

if __name__ == "__main__":
    argList = readConfig.main()
    execCmd = ["laserLab"] ##CHANGE THIS

    print("Executing Main Program...\n")
    for arg in argList:
        execCmd.append(arg.value)
    execCmd = " ".join(execCmd)

    laserLab = LaserLab(execCmd)
    worker = threading.Thread(target=laserLab.realtimeOutput)
    worker.start()

    try:
        while worker.isAlive():
            time.sleep(0.5)
    except KeyboardInterrupt:
        print
        print "Got Interrupt, closing."
        laserLab.killProc()
        os.kill(os.getpid(), 9)
    