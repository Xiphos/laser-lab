<h1>Introduction</h1>
This is a laser lab project made for the ECE 150 class taught by Paul Ward. I am extending it with various features and improvements to gain a deeper understanding of embedded systems, linux, and circuits. I also have a blog regarding this project that I plan to update daily at [mtxn.ca](http://www.mtxn.ca).

<h2>Table of Contents</h2>  

1.  [Building the Code](#building)
2.  [the Configuration File](#config)  
    I.  [the Python Script](#pythonconfig)  
    II.  [the File Syntax](#configsyntax)

<a name='building'></a>
<h1>Building the Code</h1>
There are three options for building with the makefile.  

*  <b> make </b> simply compiles the program
*  <b> make send </b> compiles and sends it to the ecelinux4 server
*  <b> make sendpi </b> compiles and sends it to the pi (pi@raspberrypi.local)  

<a name='config'></a>
<h1>The Configuration File</h1>
The reading of the configuration file is written in python, and allows for some flexibility in the syntax (as opposed to the original version). 
This code is contained in the readConfig.py script, and its details are described below. The various default filepaths are listed in the <code>LaserLab.h</code> file, 
and the python script reads this to see what the C file is using. 

<a name='pythonconfig'></a>
<h3>The Python Script</h3>
The python script aims to be as flexible and easy to update as possible. It will get the config file path from the <code>LaserLab.h</code> header, in the <code>#define CONFIGFILEPATH "[path]"</code> line. <code>readConfig.py</code> contains a list of <code>token</code> objects, defined in <code>configToken.py</code>. Each of these has the following three members:    

*  <b>name</b>: the name of the parameter
*  <b>pattern</b>: a regex pattern that represents how the config file line should be written to set the parameter
*  <b>value</b>: the value of the parameter, as matched through the <code>.validate()</code> method

To add new parameters, derive the <code>token</code> class if necessary, and add a new object in the <code>readConfig.py</code> main function.

<a name='configsyntax'></a>
<h3>The File Syntax</h3>
It is very simple to set a value, just write the parameter name (case insensitive) followed by = [value]. There can be any amount of whitespace before and after the equals sign. Current list of parameters:  

*  <b>Watchdog_Timeout [1-15]</b> - set the timeout for the watchdog (in seconds)
*  <b>Logfile</b> - set the path for the logfile (<b> NOTE TO SELF: </b> append a datestamp to the name)
*  <b>Statsfile</b> - set the path for the stats
*  <b>Outputmode [none, basic, full]</b> - set what level of output to receive