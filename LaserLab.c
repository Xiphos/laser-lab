#include "LaserLab.h"

int main(const int argc, char* argv[]){

    //Variables that will be set by config file
    int timeout;
    char* logFileName;
    char* statsFileName;
    OutputMode outputMode = BASIC;

    char cTime[30];

    //This program is not meant to be run on its own. There is a bash script that will pass in the parameters (or they can be passed in manually)
    if(argc == 1){
        fprintf(stderr, "ERROR: No config parameters passed (did you mean to run the laserLab script?)\n");
        return 1;
    }

    //They passed in the wrong number of configuration parameters
    //Plus one for the program name itself
    if(argc < NUMBEROFCONFIGPARAMETERS + 1 || argc > NUMBEROFCONFIGPARAMETERS + 1){
        fprintf(stderr, "ERROR: Expected %i config parameters, received %i\n", NUMBEROFCONFIGPARAMETERS, argc);
        return 1;
    }

    //We got the right parameters (hopefully correct and from the python script, otherwise we will crash)
    else{
        //Check for defaults first, then set the parameter (the python script will put "None")
        //For now its fine to do these as if/elses, but in the future this should be an array/dict
        if(strcmp(argv[1], CONFIGPARAMETERDEFAULT) == 0){
            timeout = WATCHDOGTIMEOUT_DEFAULT;
        }
        else{
            timeout = atoi(argv[1]); //Set the timeout
        }

        if(strcmp(argv[2], CONFIGPARAMETERDEFAULT) == 0){
            logFileName = LOGFILEPATH;
        }
        else{
            logFileName = argv[2]; //Set the logfile name
        }

        if(strcmp(argv[3], CONFIGPARAMETERDEFAULT) == 0){
            statsFileName = STATSFILEPATH;
        }
        else{
            statsFileName = argv[3]; //Set the statsfile name
        }

        if(strcmp(argv[4], CONFIGPARAMETERDEFAULT) == 0){
            outputMode = OUTPUTMODEDEFAULT;
        }
        else{
            outputMode = (OutputMode)(atoi(argv[4])); //set the outputmode
        }
    }

    //Let the user know what is going on 
    fprintf(stderr, "Config success, running with parameters:\n");
    fprintf(stderr, "Watchdog: %i Logfile: %s Statsfile: %s OutputMode: %i\n", timeout, logFileName, statsFileName, outputMode);

    //Mostly we will take the input parameters on faith, but if the watchdog value is out of bounds it might mess things up
    if(timeout > WATCHDOGTIMEOUT_MAX || timeout < WATCHDOGTIMEOUT_MIN){
        fprintf(stderr, "FATAL ERROR: Watchdog timeout was out of bounds!\n");
    }

    //We are good to go, initialise the gpio
    wiringPiSetup();

    ////////////////////////////////////////////////////
    //////            Open Log File              ///////

    fprintf(stderr, "Opening log file at %s\n", logFileName);
    //Overwrite any other logfile with the same name
    FILE* logFile = fopen(logFileName, "wb");
    if(!logFile){
        fprintf(stderr, "The log file could not be opened!\n");
        return -1;
    }

    //////           Log File Done              ///////
    ///////////////////////////////////////////////////

    ////////////////////////////////////////////////////
    //////            Set Up Watchdog            ///////


    //Uncomment the second line to re-enable watchdog capability
    int watchdog=0;
    //watchdog = setupWatchdog(timeout, cTime, logFile, outputMode);
    if(watchdog < 0){
        fprintf(stderr, "FATAL ERROR: Could not open watchdog device!\n");
        return -1;
    }

    //////            Watchdog Done              ///////
    ////////////////////////////////////////////////////

    ///////////////////////////////////////////////////
    ///////////        Function Set-Up        /////////
    int numIn=0, numOut=0;
    State funcState = START;

    int l1State = 0,l2State = 0;
    int l1Broken = 0, l2Broken = 0;
    pinMode(LASER1, INPUT);
    pinMode(LASER2, INPUT);

    //////////////////////////////////////////////////////////
    ///////////     Status Update Variables       ////////////
    int statsTime = time(0);
    int watchdogPingTime = time(0);
    int statusUpdateTime = time(0);
    int statisticsUpdates = 0;
    int watchdogPings = 0;

    while(1){
        //Get the states at the beginning of each loop
        l1State = digitalRead(LASER1);
        l2State = digitalRead(LASER2);
        //printAllGpio();

        //Update the stats occasionally
        if(difftime(time(0), statsTime) > STATISTICSUPDATE){
            statsTime = time(0);
            bool statsOutput = outputStats(statsFileName ,l1Broken, l2Broken, numIn, numOut, cTime);
        
            getTime(cTime);
            if(statsOutput){
                //Increment the number of times we have updated the statistics
                statisticsUpdates++;
            }
            else{
                //Something went wrong with the stats file
                PRINT_MSG(logFile, cTime, "ERROR: Tried to write to statistics file, but failed.", outputMode);
            }
        }

        //Ping the watchdog twice as often as the watchdog would kill us
        if(difftime(time(0), watchdogPingTime) > timeout/2){        
            ioctl(watchdog, WDIOC_KEEPALIVE, 0);
            watchdogPingTime = time(0);
            watchdogPings++;

            //Output what we did to log
            getTime(cTime);
            //PRINT_MSG(logFile, cTime, "Debug: Watchdog was kicked.");
        }

        //Give a status update to log once in a while
        if(difftime(time(0), statusUpdateTime) > STATUSUPDATE){
            //Reset time
            statusUpdateTime = time(0);
            
            //Write to logfile
            getTime(cTime);
            fprintf(logFile, "(%s) Watchdog pings: %i | Stats updates: %i | since last status update\n", cTime, watchdogPings, statisticsUpdates);\
            fflush(logFile);

            //Reset pings
            statisticsUpdates = 0;
            watchdogPings = 0;
        }

        //State machine
        getTime(cTime);
        switch(funcState){
            case START:
                //No lasers are broken, what happens next?
                usleep(SLEEPLENGTH);

                //Laser 1 breaks
                if(l1State == BROKEN && l2State == UNBROKEN){   
                    l1Broken++;
                    funcState = L1BROKEN;
                    PRINT_MSG(logFile, cTime, "State Update: x|", outputMode);
                    break;
                }

                //Laser 2 breaks
                if(l1State == UNBROKEN && l2State == BROKEN){
                    l2Broken++;
                    funcState = L2BROKEN;
                    PRINT_MSG(logFile, cTime, "State Update: |x", outputMode);
                    break;
                }

                break;
            case L1BROKEN:
                //Laser 1 is broken, what happens next?
                usleep(SLEEPLENGTH);

                //Laser 1 unbreaks
                if(l1State == UNBROKEN && l2State == UNBROKEN){
                    funcState = START;
                    PRINT_MSG(logFile, cTime, "State Update: ||", outputMode);
                    break;
                }

                //Laser 2 breaks
                else if(l1State == BROKEN && l2State == BROKEN){
                    l2Broken++;
                    funcState = PARTIALIN;
                    PRINT_MSG(logFile, cTime, "State Update: xx", outputMode);
                    break;
                }
                break;
            case L2BROKEN:
                //Laser 2 is broken, what happens next?
                usleep(SLEEPLENGTH);

                //Laser 2 unbreaks
                if(l2State == UNBROKEN && l1State == UNBROKEN){
                    funcState = START;
                    PRINT_MSG(logFile, cTime, "State Update: ||", outputMode);
                    break;
                }

                //Laser 1 breaks
                else if(l2State == BROKEN && l1State == BROKEN){
                    l1Broken++;
                    funcState = PARTIALOUT;
                    PRINT_MSG(logFile, cTime, "State Update: xx", outputMode);
                    break;
                }
                break;
            case PARTIALIN:
                //Both are broken, we are possibly on the way in. What happens next?
                usleep(SLEEPLENGTH);

                //Laser 1 is unbroken, we are now further in
                if(l1State== UNBROKEN && l2State == BROKEN){
                    funcState = GOINGIN;
                    PRINT_MSG(logFile, cTime, "State Update: |x", outputMode);
                    break;
                }

                //Laser 2 is unbroken, we are going back out
                else if(l1State == BROKEN && l2State == UNBROKEN){
                    funcState = L1BROKEN;
                    PRINT_MSG(logFile, cTime, "State Update: x|", outputMode);
                    break;
                }
                break;
            case PARTIALOUT:
                //Both are broken, we are possibly on the way out. What happens next?
                usleep(SLEEPLENGTH);

                //Laser 2 is unbroken, we are now further out
                if(l1State == BROKEN && l2State == UNBROKEN){
                    funcState = GOINGOUT;
                    PRINT_MSG(logFile, cTime, "State Update: x|", outputMode);
                    break;
                }

                //Laser 1 is unbroken, we are going back in
                else if(l1State == UNBROKEN && l2State == BROKEN){
                    funcState = L2BROKEN;
                    PRINT_MSG(logFile, cTime, "State Update: |x", outputMode);
                    break;
                }
                break;
            case GOINGIN:
                //We are one laser break away from going fully in. What happens next?
                usleep(SLEEPLENGTH);

                //Laser 2 also unbreaks, we have successfully gone in! go back to start state
                if(l1State == UNBROKEN && l2State == UNBROKEN){
                    funcState = START;
                    numIn++;
                    PRINT_MSG(logFile, cTime, "State Update: ||    [SUCCESSFUL ENTRANCE]", outputMode);
                    break;
                }

                //Laser 2 is still broken and laser 1 breaks, we are going back out
                if(l1State == BROKEN && l2State == BROKEN){
                    l1Broken++;
                    funcState = PARTIALIN;
                    PRINT_MSG(logFile, cTime, "State Update: xx", outputMode);
                    break;
                }
                break;
            case GOINGOUT:
                //We are one laser break away from fully going out. What happens next?
                usleep(SLEEPLENGTH);

                //Laser 1 also unbreaks, we have successfuly left! go back to start state
                if(l1State == UNBROKEN && l2State == UNBROKEN){
                    funcState = START;
                    numOut++;
                    PRINT_MSG(logFile, cTime, "State Update: ||    [SUCCESSFUL EXIT]", outputMode);
                    break;
                }

                //Laser 1 is still broken and laser 2 breaks, we are going back in
                if(l1State == BROKEN && l2State == BROKEN){
                    l2Broken++;
                    funcState = PARTIALOUT;
                    PRINT_MSG(logFile, cTime, "State Update: xx", outputMode);
                    break;
                }
                break;
            default:
                PRINT_MSG(logFile, cTime, "FATAL ERROR: Invalid state in main loop", outputMode);
                break;
        }
    }

    //Close the watchdog
    /*
    write(watchdog, "V", 1);
    getTime(cTime);
    PRINT_MSG(logFile, cTime, "Debug: The Watchdog was closed", outputMode);
    */

    //Close the log file
    PRINT_MSG(logFile, cTime, "Debug: Program terminating.", outputMode);
    fclose(logFile);
    return 0;
}

bool outputStats(const char* statsFile, const int l1Broken, const int l2Broken, const int numIn, const int numOut, const char* cTime){
    //////////////////////////////////////////////
    ///////        Output Stats         //////////
    FILE* stats = fopen(statsFile, "w");

    if(!stats){
        return false;
    }

    fprintf(stats, "=== Statistics for Laser Lab || Last Updated: %s ===\n", cTime);
    fprintf(stats, "|  Laser 1 was broken %i times\n", l1Broken);
    fprintf(stats, "|  Laser 2 was broken %i times\n", l2Broken);
    fprintf(stats, "|  %i objects entered the room\n", numIn);
    fprintf(stats, "|  %i objects exited the room\n\n", numOut);
    fclose(stats);

    return true;
}


void getTime(char* buffer){
    struct timeval tv;

    time_t curTime;

    gettimeofday(&tv, NULL);

    curTime = tv.tv_sec;

    strftime(buffer, 30, "%Y-%m-%d  %T", localtime(&curTime));
}

void printAllGpio(){
    //Simply prints, in one line, all the gpio values
    for(int i = 0; i < 17; i++){
        fprintf(stderr, "%i", digitalRead(i));
    }
    fprintf(stderr, "\n");
}

int setupWatchdog(int timeout, char* cTime, FILE* logFile, OutputMode om){
    int watchdog;
    getTime(cTime);

    //Try to open up the watchdog
    if((watchdog = open("/dev/watchdog", O_RDWR | O_NOCTTY)) < 0){
        PRINT_MSG(logFile, cTime, "ERROR: Could not open Watchdog device!", om);
        fclose(logFile);
        return -1;
    }
    PRINT_MSG(logFile, cTime, "Debug: The Watchdog file was successfully opened", om);

    ioctl(watchdog, WDIOC_SETTIMEOUT, &timeout);

    fprintf(logFile, "(%s) %s: %i\n", cTime, "The watchdog timer has been set to",timeout);
    fflush(logFile);

    ioctl(watchdog, WDIOC_GETTIMEOUT, &timeout);

    fprintf(stderr, "The watchdog timeout is %d seconds.\n\n", timeout);

    //Return the watchdog (used to ping it in main program)
    return watchdog;
}