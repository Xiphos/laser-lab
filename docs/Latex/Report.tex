\documentclass[]{report}
\usepackage[]{tikz}
\usepackage{listings}
\usepackage{color}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{pgf}
\usetikzlibrary{arrows,automata}
% Title Page
\title{\textbf{Embedded Lab Report}}
\author{\underline{Group 1 }\\ \\Kishen P., Matthew P., Saif K.}


\begin{document}
\maketitle
\newpage
\tableofcontents
\newpage
\chapter{Introduction}
\section{Project Overview}
This project is an embedded system that measures the number of objects entering and exiting a room. It uses a Raspberry Pi Zero along with 2 lasers to achieve this. The lasers are components of a larger breadboard circuit, whose main purpose is to send signals to the Pi GPIO when a laser breaks. The C code utilises a state machine to determine, using those signals, when an object enters or exits through the lasers. An initialized watchdog timer keeps the program running for the desired time, while concurrently, information about the program (such as broken lasers, the current state and time, and how often the watchdog was pinged) is written to a log file.

\section{High Level System Design}
\subsection{Software}
The code is written in C, compiled with the arm-linux-gnueabihf toolchain, then copied to the Raspberry Pi using a provided file server. The main part of the code follows a state-machine setup to take inputs from the photodiodes and perform the measurements (accounting for partial entrances, etc.).
\newpage
\subsection{Hardware}
There are two lasers each aimed at a photodiode. While laser light falls on these diodes, a signal of ‘1’ is sent to the Raspberry Pi’s GPIO pin (the signal that the pin receives is magnified through a system of op-amps and comparators).

When the laser beam is broken, the GPIO reads a ‘0’. By having two lasers, we can follow these signals through a predictable procedure for entrances and exits. The following figure shows the breakage steps for entrance/exit (depending on the point of view).
\newline
\newline

\begin{figure}[h]
\begin{center}
\begin{tikzpicture}
\draw [red] (0.5,0) -- (0.5,1);
\draw [red] (0.75,0) -- (0.75,1);
\draw [blue, fill=white] (0, 0.5) circle [radius=0.25];


\draw [red] (2.5,0) -- (2.5,1);
\draw [red] (2.75,0) -- (2.75,1);
\draw [blue, fill=white] (2.5, 0.5) circle [radius=0.25];

\draw [red] (4.5,0) -- (4.5,1);
\draw [red] (4.75,0) -- (4.75,1);
\draw [blue, fill=white] (4.625, 0.5) circle [radius=0.25];

\draw [red] (6.5,0) -- (6.5,1);
\draw [red] (6.75,0) -- (6.75,1);
\draw [blue, fill=white] (6.75, 0.5) circle [radius=0.25];

\draw [red] (8.5,0) -- (8.5,1);
\draw [red] (8.75,0) -- (8.75,1);
\draw [blue, fill=white] (9.25, 0.5) circle [radius=0.25];
\end{tikzpicture}
\end{center}
\caption{The series of steps required for an object to pass through the system}
\end{figure}

The state machine (described in \ref{statemachine}) follows the object through these steps, and increments a counter every time a laser is broken, and every time an object fully enters/leaves the room.

\chapter{Software Design}
\section{Overview}
The code is based on the provided gpiolib, which handles the memory mapping and basic register reading/writing functions. There are also functions within our main program, as well as an extension to the gpiolib that we created called \verb|gpiolib_pinfuncs| . A diagram of function calls follows:

\begin{figure}[h]
\begin{center}
	\includegraphics[width=.73\linewidth]{callgraph.png}
	\caption{Function call graph generated with \href{https://www.gson.org/egypt/}{\textbf{\underline{\textcolor{blue}{egypt}}}}.}
	\label{fig:call graph}
\end{center}
\end{figure}
\newpage

\section{Function Descriptions}
\subsection{Main Program Functions}
\textbf{laserDiodeStatus}
\begin{itemize}
	\item[] Purpose: Read the signal from the photodiodes
	\item[] Inputs: A gpio handle to access the appropriate registers, and an integer (1 or 2) to decide which diode to read from
	\item[] Outputs: An integer (0 or 1) representing the current status of the given diode.
\end{itemize}
\textbf{readConfig}
\begin{itemize}
	\item[] Purpose: Read the configuration file and set the watchdog timeout, as well as the locations of the log and statistics files.
	\item[] Inputs: Pointers to the values for the parameters to be set (timeout, logFileName, statsFile)
	\item[] Outputs: None, but the parameters are set within the function based on the config file
\end{itemize}
\textbf{getTime}
\begin{itemize}
	\item[] Purpose: Get the current datestamp and set it as a string for stats/logfile purposes
	\item[] Inputs: A pointer to the string we want to set the datestamp to
	\item[] Outputs: None, but the given string is set to the datestamp
\end{itemize}	
\textbf{isAlphaNumeric}
\begin{itemize}
	\item[] Purpose: Check if a character is an alphanumeric value
	\item[] Inputs: A character
	\item[] Output: Boolean value representing whether the character is alphanumeric or not
\end{itemize}
\textbf{isWhiteSpace}
\begin{itemize}
	\item[] Purpose: Check if a character is whitespace
	\item[] Inputs: A character
	\item[]	Output: Boolean value representing whether the character is whitespace or not
\end{itemize}

\newpage

\begin{flushleft}
	\textbf{isNumber}
\end{flushleft}

\begin{itemize}
	\item[]  Purpose: Check if a character is a number
	\item[]  Inputs: A character
	\item[]  Output: Boolean value representing whether the character is a number or not
\end{itemize}
\textbf{isValidValueCharacter}
\begin{itemize}
	\item[]	Purpose: Check if a character is something that is valid for a parameter value (within the context of the config file)
	\item[]	Inputs: A character
	\item[]	Output: Boolean value representing whether the character is a number or not
\end{itemize}
\textbf{tokenIsNumber}
\begin{itemize}
	\item[]	Purpose: After tokenizing, check if the string is a pure number
	\item[]	Inputs: A character array
	\item[]	Output: Boolean value representing whether the array has only numbers in it
\end{itemize}
\textbf{constructToken}
\begin{itemize}
	\item[]	Purpose: Given certain values, construct a char array for the token (within the context of the config file)
	\item[]	Inputs: A buffer representing a line in the config file, the start index of the token, and its length
	\item[]	Output: A character array containing the token
\end{itemize}
\textbf{copyParam}
\begin{itemize}
	\item[]	Purpose: Copy over the value of a parameter into the given pointer (in the context of the config file)
	\item[]	Inputs: A value token, its length, and the pointer we want to set to it
	\item[]	Output: None, but within the function the ‘to’ pointer is set to the given value
\end{itemize}
\newpage
\begin{flushleft}
	\textbf{stringEqual}
\end{flushleft}
\begin{itemize}
	\item[]	Purpose: Compare two strings, used to check if a token is equal to a parameter we want to set in the logfile (see line 544, 582, and 611 in LaserLab3.c)
	\item[]	Inputs: Two char arrays
	\item[]	Output: Boolean value representing whether the two strings are the same
\end{itemize}
\textbf{outputStats}
\begin{itemize}
	\item[]	Purpose: Write the statistics to the stats file
	\item[]	Inputs: Stats file location, and the statistics values
	\item[]	Output: None, but overwrites the stats file to reflect the updates
\end{itemize}

\subsection{gpiolib\_pinfuncs Functions}\label{pinfunc functions}

\textbf{setPin}
\begin{itemize}
	\item[]	Purpose: Set a pin to either output or input
	\item[]	Inputs: A gpio handle, a pin number, and the mode we are setting it to
	\item[]	Output: None, but writes to the appropriate gpio register
\end{itemize}
\textbf{writePin}
\begin{itemize}
	\item[]	Purpose: Write a 1 or a 0 to the given pin
	\item[]	Inputs: A gpio handle, pin number, and the value we want to write (1 or 0)
	\item[]	Output: None, but writes to the appropriate gpio register
\end{itemize}
\textbf{readPin}
\begin{itemize}
	\item[]	Purpose: Read a 1 or a 0 from the given pin
	\item[]	Inputs: A gpio handle, and a pin number
	\item[]	Output: The value received from the given pin
\end{itemize}

\section{System Dependence}
The functions that are specific to the Raspberry Pi are the functions within gpiolib, for example those listed in \ref{pinfunc functions}. Everything else is separate from the hardware. 

Given substitutes for the files/inputs/outputs, the rest of the program could function as normal. As per diagram \ref{fig:call graph}, all the connections between the main file and gpiolib are the ones that would need to be replaced to run this code on another system.
\section{The Main State Machine}\label{statemachine}

\begin{center}
\begin{tikzpicture}[->,>=stealth', shorten >=2pt, auto,node distance=6cm,
semithick]
\tikzstyle{every state}=[fill=red,draw=none,text=white]

\node[state, fill=black] (A)					{START};

\node[state]		 (B) [below left of=A]					{L1BROKEN};
\node[state,fill=teal]		 (C) [below right of=A]					{L2BROKEN};

\node[state]		(D) [below  of=B] {PARTIALIN};
\node[state,fill=teal]		(E) [below  of=C] {PARTIALOUT};

\node[state]		(F) [below of=D] {GOINGIN};
\node[state,fill=teal]		(G) [below of=E] {GOINGOUT};

\draw 
(A) edge	[bend right, above] node[align=center]{L1\\Breaks} (B)
	edge	[bend left]	node[align=center]{L2\\Breaks} (C)
	
(B) edge	[below] node[align=center, below, pos=0.3]{L1\\Unbreaks} (A)
	edge 	node[align=center]{L2\\Breaks} (D)
		 
(C) edge	node[align=center]{L2\\Unbreaks} (A)
	edge	node[align=center]{L1\\Breaks} (E) 

(D) edge	node[align=center,pos=0.2]{L1\\Unbreaks} (F)
	edge	[bend left] node[align=center]{L2\\Unbreaks} (B)
	
(E) edge	[bend left]node [align=center]{L1\\Unbreaks} (C)
	edge 	node[align=center]{L2\\Unbreaks} (G)

(F) edge	[bend left] node[align=center]{L1\\Breaks} (D)
	edge	[bend right=10] node[align=center,pos=0.1, right] {L2 Unbreaks \\ ++numOut} (A)

(G) edge	[bend left] node[align=center, pos=0.8]{L2\\Breaks} (E)
	edge	[bend left=20] node[align=center, pos=0.3] {L1 Unbreaks\\ ++numIn} (A);
\hspace*{9em}
\end{tikzpicture}
\end{center}

\section{Configuration File}
The system allows for a configuration file that can set three parameters:
\begin{itemize}
	\item[] \verb|WATCHDOG_TIMEOUT|: The timeout value for the watchdog
	\item[] \verb|LOGFILE|: The file path for the log file
	\item[] \verb|STATSFILE|: The file path for the stats file
\end{itemize}
These are set by editing the configuration file located at \verb|/home/pi/LaserLab.cfg|. To set a parameter, simply write the name (exactly as it appears in the list) followed by an equal sign, then the parameter value. Comments can appear in the log file and are preceded by a \verb|#|. Invalid/absent values for these parameters are replaced with their defaults as follows:
\begin{itemize}
	\item[] \verb|WATCHDOG_TIMEOUT|: 10 seconds
	\item[] \verb|LOGFILE|: \verb|/home/pi/LaserLab.log|
	\item[] \verb|STATSFILE|: \verb|/home/pi/LaserLab.stats|
\end{itemize}
\section{Log File}
The log file line format is as follows:\\
\verb|==<PID>==| | \verb|<Program Name>| | \verb|: <Severity>: <Message> (<Datestamp>)|\\
The log points are as follows:
\begin{itemize}
	\item[1)] When the watchdog is first opened
	\item[2)] Every \verb|STATUSUPDATE = 15| seconds with the number of times the watchdog has been pinged, and the number of times the stats file was updated
	\item[3)] Whenever a laser breaks, output what happened and what state we are going into to the log file
	\item[4)] When the program ends, to signify that shutdown went okay
\end{itemize}
The log file is written to using the \verb|PRINT_MSG| macro to keep the code cleaner and allow us to change the line format in the future.
\section{Watchdog}
After reading the configuration file, the \verb|timout| is set to a value between 1 and 15 seconds. The watchdog is pinged whenever \verb|difftime(time(0), watchdogPingTime) > timeout/2| (i.e. every half-timeout) to prevent the watchdog from killing the process. On a reboot, the process starts again and continues to update the statistics file (since it is symlinked as a service running in the background).
\chapter{Further Information}
\section{Testing}
Throughout the process of developing the project, we tested the functionality by simply moving an object through the laser setup, recording the number of laser breaks, entrances, and exits, and then compared this to the output in the statistics file. 

We also added various \verb|fprintf| statements to make sure things were running as expected on the Pi, though these are commented out in the final release. One of the benefits of writing hardware-independent code was that we were able to use a debugger to find mistakes in the code. Another benefit was that, during the early stages of writing the code, we could simulate the two lasers breaking with two pushbuttons in a much simpler circuit. These two tools that we used were only possible because of the fact that the code was created with hardware independence in mind.
\section{Miscellaneous}
\subsection{Extras}
As stated earlier, we added \verb|gpiolib_pinfuncs| which includes some functions (\ref{pinfunc functions}) that make writing to, and reading from, pins easier. The register bitfield calculations don't need to be done by hand, which makes the code simpler and cleaner, and allows for flexibility when choosing the pins for I/O.
\newpage
\subsection{Limitations}
In its current state, there is no immediate feedback for when the program is running. Adding an LED or physical display to count the items would make the user experience better, rather than having them check the log/stats file to make sure everything is right.
\subsection{Reflection}
There are a couple of improvements that we would enact if we were to do this project again. As stated previously, we would add physical displays to reflect the status of the system to make debugging easier, and also make the system more of an embedded system. 

We would also create a physical device to keep the lasers/diodes in place as we found that during testing they would constantly be a little bit off their marks, making the laser break counting inaccurate.
\chapter{Appendix}
\section{Source Code}
The source code, along with the call graph and this document, are available on \href{https://gitlab.com/Xiphos/ece-150-embedded-lab}{\textbf{\underline{\textcolor{blue}{GitLab}}}}. This includes the gpiolib files, the main file \verb|LaserLab3.c|, and the gpiolib extension \verb|gpiolib_reg.c|. 
\section{Peer Contributions}

\renewcommand{\arraystretch}{1.5}
\begin{center}
	\begin{tabular}{ | l | p{5cm}|}
		\hline
		\textbf{Member }& \textbf{Contributions}\\
		\hline
		Kishen & 
		State machine development  
		
		Circuit debugging
		\\
		\hline
		Matthew & 
		Circuit building
		
		State machine debugging
		\\
		\hline
		Saif & 
		Config/Log/Stats functionality
		
		Software debugging
		\\
		\hline
	\end{tabular}
\end{center}

\end{document}